/* DOS MOTD 
 * 
 *  (c) 2018 <stecdose@gmail.com>
 *  license: GPLv2
 */

#include <stdio.h>
#include <string.h>
#include <dos.h>

#define uint16_t    unsigned int

#define DEFAULT_MOTD  "C:\\MOTD"

int do_seq(char *seq);

int main(int argc, char *argv[]) {
    int f, i, j, linen;
    char line[514], temp[32], c;

    if(argc == 1) {     // no args given
        f = fopen(DEFAULT_MOTD, "r");
    } else if(argc == 2) {
        f = fopen(argv[1], "r");
    }

    if(f == NULL) {
        puts("can't open MOTD file for reading...\r\n");
        exit(-1);
    }

    linen = 0;
    while(fgets(line, 512, f)) {
        linen++;
        line[strlen(line)-1] = '\0';
        for(i = 0; i < strlen(line); i++) {
            c = line[i];
            if((c >= 0x20)) {       // printable chars
                putch(c);
            } else {
                switch(c) {
                    case 0x04:      // end of transmission
                        fclose(f);
                        exit(0);
                        break;
                    case 0x07:      // bell
                        break;
                    case 0x09:      // horizontal tab
                        break;
                    case 0x0A:      // line feed
                        break;
                    case 0x0B:      // vertical tab
                        break;
                    case 0x0D:      // carriage return (newline)
                        puts("");
                        break;
                    case 0x1B:      // ESCAPE
                        j = 0;
                        do {
                            c = line[i+1];
                            temp[j] = c;
                            i++;
                            j++;
                            if(j == 8) {
                                printf("------\r\n");
                                printf("incorrent escape sequence at line %d, col %d\r\n", linen, i-8);
                                exit(-1);
                            }
                        } while((c != 'm'));
                        temp[j] = '\0';
                        do_seq(temp);
                        break;
                }
            }
        }

    }
    fclose(f);

}

int do_seq(char *seq) {
    int i;

    for(i = 1; i < strlen(seq); i++) {
        seq[i-1] = seq[i];
    }
    seq[i-2] = '\0';
    
    printf("parsing sequence '%s'\r\n", seq);
    return 0;
}
